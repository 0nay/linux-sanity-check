#!/usr/bin/python3

# DTS Pro Python 2B Class
# 
# Group M Project
# 
# Active Member: Adya Imanina, Ronaldi Santosa, Padlan Alqinsi (Group N), Auliya Putra Azhari (Group T), Bais Azi Ramdhan Nur (Group R)

import os
import sys
import subprocess
import datetime
import argparse
import textwrap
import platform
from colorama import Fore, Back, Style

if platform.system() != "Linux":
  print("Script exit, this script only working on Linux!")
  exit()

if sys.version_info <= (3, 7):
  print("Script exit, this script required minimum python version 3.7!")
  exit()

parser = argparse.ArgumentParser(prog='lsancheck', formatter_class=argparse.RawDescriptionHelpFormatter, description=textwrap.dedent('''
Linux sanity checker script 

The script is use to sanitize important config after server reboot.
Need to run it before and after the reboot maintenance.'''))
parser.add_argument("-s", "--save-config", help="Run config save before server reboot", action="store_true")
parser.add_argument("-c", "--sanity-check", help="Run sanity check, matching before and after saved config", action="store_true")
parser.add_argument("-l", "--list-config", help="List saved config output on default /tmp/lsancheck directory", action="store_true")
args = parser.parse_args()

today = str(datetime.date.today())

dirpath = '/tmp/lsancheck/'
dirpathtoday = dirpath + today + '/'
dirchoosen = dirpathtoday

# List of command
uptime = ["uptime"]
uname_n = ["uname -n"]
uname_r = ["uname -r"]
ip_address = ["ip -f inet a | grep -v valid_lft"]
ip_r_sh = ["ip r sh"]
lsblk = ["lsblk"]
df_h = ["df -hx tmpfs --output=source,fstype,size,target"] 
systemctl = ["systemctl list-units --type=service --state=running"]

def command(cmd):
  output = subprocess.run(cmd, shell=True, capture_output=True)
  print(output.stdout.decode().rstrip())

def save_file(file_name, cmd):
  original_stdout = sys.stdout
  with open(dirpathtoday + file_name, 'w') as f:
    sys.stdout = f
    command(cmd)
    sys.stdout = original_stdout

def print_match():
  print("[" + Fore.GREEN + "Match".center(11) + Style.RESET_ALL +"]")

def print_not_match():
  print("[" + Fore.RED + "Not match".center(11) + Style.RESET_ALL + "]")

def print_done():
  print("[" + Fore.GREEN + "Done".center(6) + Style.RESET_ALL +"]")

if args.save_config:
  print("< " + " Saving current configuration ".center(50, "-") + " >")
  print()

  is_exist = os.path.exists(dirpath)
  if not is_exist:
    print("Directory to save config check is not exist. Creating on /tmp/lsancheck")
    print()
    os.makedirs(dirpath)

  is_exist_today = os.path.exists(dirpathtoday)
  if not is_exist_today:
    os.makedirs(dirpathtoday)

  print("Saving last uptime ... ".ljust(46), end="")
  save_file("uptime.out", uptime)
  print_done()
  print("Saving hostname output ...".ljust(46), end="")
  save_file("uname-n.out", uname_n)
  print_done()
  print("Saving kernel version ...".ljust(46), end="")
  save_file("uname-r.out", uname_r)
  print_done()
  print("Saving ip address information ...".ljust(46), end="")
  save_file("ip-a-sh.out", ip_address)
  print_done()
  print("Saving routing information ...".ljust(46), end="")
  save_file("ip-r-sh.out", ip_r_sh)
  print_done()
  print("Saving disk partition information ...".ljust(46), end="")
  save_file("lsblk.out", lsblk)
  print_done()
  print("Saving mount point information ...".ljust(46), end="")
  save_file("df-h.out", df_h)
  print_done()
  save_file("systemctl.out", systemctl)
  print("Saving running services ...".ljust(46), end="")
  print_done()
  print("Complete.")

def sanity_check(file_name, cmd, cmd_check):
  if os.path.exists(dirchoosen + file_name) == False:
    print("Saving output configuration file is not found, please run the lsancheck --save-config")
    exit()
  with open(dirchoosen + file_name) as f:
    file = f.read()
    before = file.rstrip()
  output = subprocess.run(cmd, shell=True, capture_output=True)
  after = (output.stdout.decode().rstrip())
    
  if before == after: 
    print(cmd_check.ljust(21) + ":".ljust(20), end="")
    print_match()
  else:
    print(cmd_check.ljust(21) + ":".ljust(20), end="")
    print_not_match()
  
if args.list_config:
  compare_list = os.listdir(dirpath)
  compare_list.sort(reverse=True)
  compare_length = len(compare_list)
  print("Listing saved config on /tmp/lsancheck")
  print()
  if compare_length == 0:
    print("Saving output configuration file is not found, please run the lsancheck --save-config")
    exit()
  for n, folder in enumerate(compare_list, start=0):
    print(folder)
  exit()

def compare_folder():
  compare_list = os.listdir(dirpath)
  compare_list.sort(reverse=True)
  compare_length = len(compare_list)
  if(compare_length > 1):
    print("Found", compare_length, "saved config on /tmp/lsancheck directory.".ljust(37))
    print()
    for n, folder in enumerate(compare_list,start=0):
      print("[",n,"]",end=" ")
      print(folder)
    print()
    choosen = input("Please choose which saved config to compare, default [0] : ")
    if choosen == "":
      choosen = 0
    choosen = int(choosen)
    if (choosen < 0 or choosen > compare_length - 1):
      print("The number you choose is not exist, please try again!")
      exit()
    print("Matching ", end="")
    config_choosen = compare_list[choosen]
    print(config_choosen)
    print()
    global dirchoosen 
    dirchoosen = dirpath + config_choosen + "/"
	  
  
if args.sanity_check or len(sys.argv) == 1:
  isFolderExist = os.path.exists(dirpath)
  if isFolderExist == False:
    print("For the first run, please run with --save-config flags")
    print()
    exit()
  print("< " + " Running sanity check ".center(50, "-") + " >")
  print()
  compare_folder()
  
  sanity_check("uname-n.out", uname_n, "Hostname")
  sanity_check("uname-r.out", uname_r, "Kernel version")
  sanity_check("ip-a-sh.out", ip_address, "IP Address")
  sanity_check("ip-r-sh.out", ip_r_sh, "IP Route")
  sanity_check("lsblk.out", lsblk, "Disk Partition")
  sanity_check("df-h.out", df_h, "Mount point")
  sanity_check("systemctl.out", systemctl, "Running service")
  print("Complete.")
