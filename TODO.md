TO-DO:

1. Add date on dir name e.g 2021-06-22-0905
2. Input on save config, ask user if want to save the config on other folder?
3. Add direct args for sanity check e.g lsancheck.py -c "folder_name"
4. Add more exceptions handling on code e.g only allow number on choosen directory
5. Add report feature
6. Add compress feature for saved folder
7. Add sent to email for report
