# Please check on new_feature branch for the latest script update

# linux-sanity-check

usage: lsancheck [-h] [-s] [-c] [-l]

Linux sanity checker script 

The script is use to sanitize important config after server reboot.
Need to run it before and after the reboot maintenance.

optional arguments:
  -h, --help          show this help message and exit
  -s, --save-config   Run config save before server reboot
  -c, --sanity-check  Run sanity check, matching before and after saved config
  -l, --list-config   List saved config output on default /tmp/lsancheck
                      directory
